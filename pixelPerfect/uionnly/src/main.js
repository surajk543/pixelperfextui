import Vue from 'vue';
import App from './App.vue';
import VueRouter from 'vue-router';
Vue.config.productionTip = false;

import Buefy from 'buefy';
import 'buefy/dist/buefy.css';
Vue.use(Buefy);
Vue.use(VueRouter);

import Screen1 from './components/Screen1/Landingpage.vue';

const routes = [
  {
    path:'/',
    component:Screen1,
    name:'Screen1'
  }
];
const router = new VueRouter({
  routes,
  mode: 'history'
});
new Vue({
  render: h => h(App),
  router,
}).$mount('#app')
